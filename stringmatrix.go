package main

type StringMatrix []StringList

func MakeStringMatrix(rows ...StringList) StringMatrix {
	return StringMatrix(rows)
}

// Erwartet eine Funktion mit StringList-Pointer als Parameter und wendet diese auf jedes Element in matrix an.
// Diese Funktion kann matrix In-Place verändern.
func (matrix StringMatrix) ForEachElement(f func(*StringList)) {
	for i := range matrix {
		f(&matrix[i])
	}
}

// Erwartet eine Funktion mit StringList als Parameter und führt diese mit jedem Element von matrix aus.
// Diese Funktion kann matrix nicht verändern. Die Funktion f sollte Variablen einfangen oder andere Seiteneffekte haben.
func (matrix StringMatrix) ForEachValue(f func(StringList)) {
	for _, value := range matrix {
		f(value)
	}
}

// Wendet f auf jedes Element von matrix an. Liefert die resultierende StringMatrix.
func (matrix StringMatrix) Map(f func(StringList) StringList) StringMatrix {
	result := MakeStringMatrix()
	matrix.ForEachValue(func(row StringList) { result = append(result, f(row)) })
	return result
}

// Wendet pred auf jedes Element von matrix an. Liefert die StringMatrix, die nur aus den
// Elementen vpn matrix besteht, für die pred true liefert.
func (matrix StringMatrix) Filter(pred func(StringList) bool) StringMatrix {
	result := MakeStringMatrix()
	matrix.ForEachValue(func(row StringList) {
		if pred(row) {
			result = append(result, row)
		}
	})
	return result
}

// Akkumuliert die Zwischenergebnisse von op mit allen Matrix-Zeilen, mit Startwert start.
func (matrix StringMatrix) Reduce(op func(StringList, StringList) StringList, start StringList) StringList {
	result := start
	matrix.ForEachValue(func(row StringList) { result = op(result, row) })
	return result
}

// Liefert die angegebene Zeile der Matrix.
func (matrix StringMatrix) Row(row int) StringList {
	return matrix[row]
}

// Liefert die angegebene Spalte der Matrix.
func (matrix StringMatrix) Column(col int) StringList {
	return matrix.Reduce(func(result, line StringList) StringList { return append(result, line[col]) }, MakeStringList())
}

// Liefert die Transponierte der Matrix.
func (matrix StringMatrix) Transpose() StringMatrix {
	result := MakeStringMatrix()
	if len(matrix) > 0 {
		for col := range matrix.Row(0) {
			result = append(result, matrix.Column(col))
		}
	}
	return result
}

// Liefert true, falls pred für irgend eine Zeile von matrix gilt.
func (matrix StringMatrix) AnyRow(pred func(StringList) bool) bool {
	result := false
	matrix.ForEachValue(func(row StringList) { result = result || pred(row) })
	return result
}

// Liefert true, falls pred für jede Zeile von matrix gilt.
func (matrix StringMatrix) AllRows(pred func(StringList) bool) bool {
	result := false
	matrix.ForEachValue(func(row StringList) { result = result && pred(row) })
	return result
}

// Liefert true, falls pred für irgend eine Spalte von matrix gilt.
func (matrix StringMatrix) AnyColumn(pred func(StringList) bool) bool {
	result := false
	matrix.Transpose().ForEachValue(func(row StringList) { result = result || pred(row) })
	return result
}

// Liefert true, falls pred für jede Spalte von matrix gilt.
func (matrix StringMatrix) AllColumns(pred func(StringList) bool) bool {
	result := false
	matrix.Transpose().ForEachValue(func(row StringList) { result = result && pred(row) })
	return result
}

// Wendet die Funktion f auf jedes einzelne Element der Matrix an.
func (matrix StringMatrix) MapToEachString(f func(string) string) StringMatrix {
	return matrix.Map(func(sl StringList) StringList { return sl.Map(f) })
}
