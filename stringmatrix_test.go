package main

import (
	"fmt"
	"strings"
)

func exampleMatrix1() StringMatrix {
	l1 := MakeStringList("a", "b")
	l2 := MakeStringList("c", "d")
	return MakeStringMatrix(l1, l2)
}

func ExampleStringMatrixForEachElement() {
	m1 := exampleMatrix1()

	m1.ForEachElement(func(*StringList) { fmt.Println("I see a StringList.") })
	m1.ForEachElement(func(l *StringList) { *l = append(*l, "ABC") })
	fmt.Println(m1)
	// Output:
	// I see a StringList.
	// I see a StringList.
	// [[a b ABC] [c d ABC]]
}

func ExampleStringMatrixForEachValue() {
	m1 := exampleMatrix1()

	m1.ForEachValue(func(StringList) { fmt.Println("I see a StringList.") })
	m1.ForEachValue(func(l StringList) { l = append(l, "ABC") })
	fmt.Println(m1)
	// Output:
	// I see a StringList.
	// I see a StringList.
	// [[a b] [c d]]
}

func ExampleStringMatrixMap() {
	m1 := exampleMatrix1()

	appendAToS := func(s string) string { return s + "A" }
	appendAToAllS := func(l StringList) StringList { return l.Map(appendAToS) }

	fmt.Println(m1.Map(appendAToAllS))

	// Output:
	// [[aA bA] [cA dA]]
}

func ExampleStringMatrixFilter() {
	m1 := exampleMatrix1()

	isA := func(s string) bool { return s == "a" }
	anyIsA := func(s StringList) bool { return s.Any(isA) }

	fmt.Println(m1.Filter(anyIsA))

	// Output:
	// [[a b]]

}

func ExampleStringMatrixReduce() {
	m1 := exampleMatrix1()

	fmt.Println(m1.Reduce(func(l1, l2 StringList) StringList { return append(l1, l2...) }, MakeStringList()))

	// Output:
	// [a b c d]
}

func ExampleStringMatrixRow() {
	m1 := exampleMatrix1()

	fmt.Println(m1.Row(0))
	fmt.Println(m1.Row(1))

	// Output:
	// [a b]
	// [c d]
}

func ExampleStringMatrixColumn() {
	m1 := exampleMatrix1()

	fmt.Println(m1.Column(0))
	fmt.Println(m1.Column(1))

	// Output:
	// [a c]
	// [b d]
}

func ExampleStringMatrixTranspose() {
	m1 := exampleMatrix1()
	m2 := MakeStringMatrix()

	fmt.Println(m1.Transpose())
	fmt.Println(m2.Transpose())

	// Output:
	// [[a c] [b d]]
	// []
}

func ExampleStringMatrixAnyRow() {
	m1 := exampleMatrix1()

	stringContainsA := func(s string) bool { return strings.Contains(s, "a") }
	stringListContainsA := func(row StringList) bool { return row.Any(stringContainsA) }

	fmt.Println(m1.AnyRow(stringListContainsA))

	// Output:
	// true
}
func ExampleStringMatrixAllRows() {
	m1 := exampleMatrix1()

	stringContainsA := func(s string) bool { return strings.Contains(s, "a") }
	stringListContainsA := func(row StringList) bool { return row.Any(stringContainsA) }

	fmt.Println(m1.AllRows(stringListContainsA))

	// Output:
	// false
}

func ExampleStringMatrixAnyColumn() {
	m1 := exampleMatrix1()

	stringContainsA := func(s string) bool { return strings.Contains(s, "a") }
	stringListContainsA := func(col StringList) bool { return col.Any(stringContainsA) }

	fmt.Println(m1.AnyColumn(stringListContainsA))

	// Output:
	// true
}
func ExampleStringMatrixAllColumns() {
	m1 := exampleMatrix1()

	stringContainsA := func(s string) bool { return strings.Contains(s, "a") }
	stringListContainsA := func(col StringList) bool { return col.Any(stringContainsA) }

	fmt.Println(m1.AllColumns(stringListContainsA))

	// Output:
	// false
}

func ExampleMapToEachString() {
	m1 := exampleMatrix1()
	appendAToS := func(s string) string { return s + "A" }

	fmt.Println(m1.MapToEachString(appendAToS))

	// Output:
	// [[aA bA] [cA dA]]
}
