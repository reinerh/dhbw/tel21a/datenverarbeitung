package main

import (
	"bufio"
	"os"
)

// Erwartet den Namen einer Textdatei und liefert eine Liste von Strings, jeder String eine Zeile der Datei.
func ReadLinesFromFile(filename string) StringList {
	file, error := os.Open(filename)

	// Fehlerüberprüfung: Wenn die Datei nicht geöffnet werden konnte, eine leere Liste liefern.
	if error != nil {
		return MakeStringList()
	}

	// Die Datei am Ende dieser Funktion wieder schließen.
	// Mit defer können wir den Aufruf direkt hier hinschreiben und sorgen dafür, dass er auf jeden Fall geschieht,
	// sobald die Funktion beendet wird. Das geschieht sogar dann, wenn die Funktion z.B. durch eine Panic endet.
	defer file.Close()

	// Einen Scanner erzeugen, der seine Eingabe aus der soeben geöffneten Datei liest.
	// Ein Scanner ist ein Objekt, das eine Eingabe zeilenweise verarbeiten kann.
	scanner := bufio.NewScanner(file)

	// Die eigentliche Verarbeitung der Datei:
	lines := MakeStringList()
	for scanner.Scan() { // Liest eine Zeile. Wird als false interpretiert, sobald das Ende der Datei erreicht ist.
		lines = append(lines, scanner.Text()) // Die zuletzt gelesene Zeile an unsere Ergebnisliste anhängen.
	}
	return lines
}
