package main

import (
	"fmt"
	"strings"
)

func main() {
	Beispiel1()
	Beispiel2()
	Beispiel3()
}

// Beispiel 1: Die Datei test1.txt einlesen.
// Dort enthält jede Zeile genau einen String, diese Strings geben wir direkt aus.
func Beispiel1() {
	fmt.Println(ReadLinesFromFile("test1.txt"))
}

// Beispiel 2: Die Datei test2.txt einlesen.
// Dort ist jede Zeile von der Form "a: 1". Wir lesen diese Zeilen und splitten sie dann an den Doppelpunkten.
// Das Ergebnis ist eine zweidimensionale Slice aus Strings, die die Doppelpunkte nicht mehr enthält.
func Beispiel2() {
	lines := ReadLinesFromFile("test2.txt")
	splitLine := func(line string) StringList { return strings.Split(line, ":") }
	matrix := lines.MapStringsToStringLists(splitLine)
	matrix = matrix.MapToEachString(strings.TrimSpace)
	fmt.Println(matrix)
}

// Beispiel 3: Daten wie in Beispiel 2 einlesen und splitten.
// Dazu wird dieses Mal die Methode SplitStrings() von StringList verwendet.
// Anschließend nur die zweite Spalte herausfiltern.
func Beispiel3() {
	splitLines := ReadLinesFromFile("test2.txt").SplitStrings(":")
	column1 := splitLines.Column(1)
	fmt.Println(column1)
}

// Beispiel 4: Daten wie in Beispiel 2 und 3 einlesen, splitten und die Spalte 2 herausfiltern.
// Dazu wird dieses Mal die Methode Column() von StringMatrix verwendet.
func Beispiel4() {
	splitLines := ReadLinesFromFile("test2.txt").SplitStrings(":").MapToEachString(strings.TrimSpace)
	column1 := splitLines.Column(1)
	fmt.Println(column1)
}
