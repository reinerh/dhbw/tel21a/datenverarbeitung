package main

import "strings"

type StringList []string

// Erzeugt eine neue StringList aus den angegebenen Strings.
func MakeStringList(strings ...string) StringList {
	return StringList(strings)
}

// Erwartet eine Funktion mit String-Pointer als Parameter und wendet diese auf jedes Element in list an.
// Diese Funktion kann list In-Place verändern.
func (list StringList) ForEachElement(f func(*string)) {
	for i := range list {
		f(&list[i])
	}
}

// Erwartet eine Funktion mit String als Parameter und führt diese mit jedem Element von list aus.
// Diese Funktion kann list nicht verändern. Die Funktion f sollte Variablen einfangen oder andere Seiteneffekte haben.
func (list StringList) ForEachValue(f func(string)) {
	for _, v := range list {
		f(v)
	}
}

// Wendet f auf jedes Element von list an. Liefert die resultierende StringList.
func (list StringList) Map(f func(s string) string) StringList {
	result := MakeStringList()
	list.ForEachValue(func(value string) { result = append(result, f(value)) })
	return result
}

// Wendet pred auf jedes Element von list an. Liefert die StringList, die nur aus den
// Elementen vpn list besteht, für die pred true liefert.
func (list StringList) Filter(pred func(s string) bool) StringList {
	result := MakeStringList()
	list.ForEachValue(func(value string) {
		if pred(value) {
			result = append(result, value)
		}
	})
	return result
}

// Akkumuliert die Zwischenergebnisse von op mit allen Listenelementen, mit Startwert start.
func (list StringList) Reduce(op func(string, string) string, start string) string {
	result := start
	list.ForEachValue(func(value string) { result = op(result, value) })
	return result
}

// Liefert true, falls pred für irgend ein Element in list true liefert.
func (list StringList) Any(pred func(s string) bool) bool {
	result := false
	list.ForEachValue(func(s string) { result = result || pred(s) })
	return result
}

// Liefert true, falls pred für alle Elemente in list true liefert.
func (list StringList) All(pred func(s string) bool) bool {
	return !list.Any(func(s string) bool { return !pred(s) })
}

func (list StringList) MapStringsToStringLists(f func(s string) StringList) StringMatrix {
	result := MakeStringMatrix()
	list.ForEachValue(func(s string) { result = append(result, f(s)) })
	return result
}

func (list StringList) SplitStrings(sep string) StringMatrix {
	splitter := func(line string) StringList { return StringList(strings.Split(line, sep)) }
	return list.MapStringsToStringLists(splitter)
}
