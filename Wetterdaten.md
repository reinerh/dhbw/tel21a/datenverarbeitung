# Temperaturen

## 01.01.
* Morgens: -2 Grad
* Mittags: 3 Grad
* Abends: 2 Grad

## 01.02.
* Morgens: 5 Grad
* Mittags: 20 Grad
* Abends: 4 Grad

## 01.03.
* Morgens: 3 Grad
* Mittags: 13 Grad
* Abends: 2 Grad