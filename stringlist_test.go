package main

import (
	"fmt"
	"strings"
)

func ExampleStringListForEachElement() {
	l1 := MakeStringList("Hallo", "Welt")
	l2 := MakeStringList("abc", "ayz")

	l1.ForEachElement(func(*string) { fmt.Println("I see a string.") })
	l1.ForEachElement(func(s *string) { *s += "A" })
	fmt.Println(l1)
	l2.ForEachElement(func(*string) { fmt.Println("I see a string.") })
	l2.ForEachElement(func(s *string) { *s += "A" })
	fmt.Println(l2)

	// Output:
	// I see a string.
	// I see a string.
	// [HalloA WeltA]
	// I see a string.
	// I see a string.
	// [abcA ayzA]
}

func ExampleStringListForEachValue() {
	l1 := MakeStringList("Hallo", "Welt")
	l2 := MakeStringList("abc", "ayz")

	l1.ForEachValue(func(string) { fmt.Println("I see a string.") })
	l1.ForEachValue(func(s string) { s += "A" })
	fmt.Println(l1)
	l2.ForEachValue(func(string) { fmt.Println("I see a string.") })
	l2.ForEachValue(func(s string) { s += "A" })
	fmt.Println(l2)

	// Output:
	// I see a string.
	// I see a string.
	// [Hallo Welt]
	// I see a string.
	// I see a string.
	// [abc ayz]
}

func ExampleStringListMap() {
	l1 := MakeStringList("Hallo", "Welt")
	l2 := MakeStringList("abc", "ayz")

	replaceAByE := func(s string) string { return strings.ReplaceAll(s, "a", "e") }
	fmt.Println(l1.Map(replaceAByE))
	fmt.Println(l2.Map(replaceAByE))

	// Output:
	// [Hello Welt]
	// [ebc eyz]
}

func ExampleStringListFilter() {
	l1 := MakeStringList("Hallo", "Welt")
	l2 := MakeStringList("abc", "ayz")

	containsA := func(s string) bool { return strings.ContainsAny(s, "a") }
	fmt.Println(l1.Filter(containsA))
	fmt.Println(l2.Filter(containsA))

	// Output:
	// [Hallo]
	// [abc ayz]
}

func ExampleStringListReduce() {
	l1 := MakeStringList("Hallo", "Welt")
	l2 := MakeStringList("abc", "ayz")

	concat := func(s1, s2 string) string { return s1 + s2 }
	fmt.Println(l1.Reduce(concat, ""))
	fmt.Println(l1.Reduce(concat, "ABC"))
	fmt.Println(l2.Reduce(concat, ""))
	fmt.Println(l2.Reduce(concat, "ABC"))

	// Output:
	// HalloWelt
	// ABCHalloWelt
	// abcayz
	// ABCabcayz
}

func ExampleStringListAny() {
	l1 := MakeStringList("Hallo", "Welt")
	l2 := MakeStringList()

	fmt.Println(l1.Any(func(s string) bool { return strings.Contains(s, "a") }))
	fmt.Println(l1.Any(func(s string) bool { return strings.Contains(s, "l") }))
	fmt.Println(l1.Any(func(s string) bool { return strings.Contains(s, "z") }))
	fmt.Println(l2.Any(func(s string) bool { return strings.Contains(s, "z") }))

	// Output:
	// true
	// true
	// false
	// false
}
func ExampleStringListAll() {
	l1 := MakeStringList("Hallo", "Welt")
	l2 := MakeStringList()

	fmt.Println(l1.All(func(s string) bool { return strings.Contains(s, "a") }))
	fmt.Println(l1.All(func(s string) bool { return strings.Contains(s, "l") }))
	fmt.Println(l1.All(func(s string) bool { return strings.Contains(s, "z") }))
	fmt.Println(l2.All(func(s string) bool { return strings.Contains(s, "z") }))

	// Output:
	// false
	// true
	// false
	// true
}
